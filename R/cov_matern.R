#' Matérn covariance function of order 3/2
#'
#' @param d a matrix/vector/scalar. Distances
#' @param sill positive scalar. Sill variance parameter. Default to 1.
#' @param range positive scalar. Range parameter controlling correlation decay with distance.
#'  Default to 1 so that the correlation is 0.5 for d = 1.
#'
#' @importFrom assertthat assert_that is.scalar
#'
#' @return A covariance
#' @export
#'
#' @examples
#' cov_matern(d = 1)
#' cov_matern(d = 0:10)
#' cov_matern(d = matrix(c(0, 1, 1, 0), nrow = 2, byrow = TRUE))
cov_matern <- function(d,
                       sill = 1,
                       range = 1
                       ) {
  ### sanity checks
  assert_that(is.matrix(d) || is.vector(d) || is.scalar(d))
  assert_that(all(d >= 0))
  assert_that(is.scalar(sill) && sill >= 0)
  assert_that(is.scalar(range) && range > 0)

  return(sill * (1 + d * sqrt(3) / range) * exp(- d * sqrt(3) / range))
}
