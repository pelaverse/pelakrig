
## Package installation

``` r
install.packages("remotes")
remotes::install_gitlab(
  host = "https://gitlab.univ-lr.fr",
  repo = "pelaverse/pelakrig",
  build_vignettes = TRUE
)
```

## Use of the package

Functions of pelaKrig are presented in a vignette, with an application
on data available in the package :

``` r
vignette("Kriging_analysis")
```
