---
title: "Kriging_analysis"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Kriging_analysis}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.align = "center",
  fig.width = 7, 
  fig.height = 7
)
```

## Prerequisite

To use the functions of **pelaKrig**, input data must have a standardized format with the following columns :

-   **X** : longitude coordinates
-   **Y** : latitude coodinates
-   **Transect.Label** : identifier of the leg
-   **Seg** : segment identifier of the corresponding leg
-   **seg_id** : unique segment identifier corresponding of merge of Transect.Label and Seg
-   **n** : number of detection
-   **y** : group size
-   **esw** : effective strip width determined by CDS analysis

This data corresponds to **segdata_obs** built with [pelaDSM](https://gitlab.univ-lr.fr/pelaverse/peladsm) package.

## Function organisation

Functions have to be used in a precise order, this is develop in the next figure :

1.  `variogram_intraTransect()` : Estimation of semi-variance from Poisson

2.  `fit_variomodel()` : Adjusting function on semi-variance distribution

3.  `pois_kriging()` : Poisson kriging Monestiez way

    ![](images/kriging_function_architecture-01.png){width="50%"}

`variogram_intraTransect()` and `pois_kriging()` have `intraTransect` argument which allows to take into account semi-variance between points belonging to the same transect if `intraTransect = TRUE` or between all points whether their belong to the same transect or not if `intraTransect = FALSE`. This can be useful, if points have been sampled at different time.

## Application

An artificial data (`simul_data`) stored in the package was created to show how to use functions.

### Visualization of `simul_data`

```{r message=FALSE, results='asis', out.width='45%', fig.align='default', fig.show='hold'}
library(pelaKrig)
library(ggplot2)
library(dplyr)

custom_theme <-  theme_bw() +
    theme(axis.title=element_blank(),
          axis.text=element_blank(),
          axis.ticks=element_blank())


simul_data %>% 
  ggplot() +
  geom_point(aes(x = X, y = Y,  colour = n, size = n)) +
  labs(title = "Number of detection") +
  scale_colour_viridis_c() +
  custom_theme

simul_data %>% 
  ggplot() +
  geom_point(aes(x = X, y = Y, colour = y, size = y)) +
  labs(title = "Number of individuals") +
  scale_colour_viridis_c() +
  custom_theme

```

It seems that the distribution is not homogeneous and patches can be distinguished, so a kriging analysis can be performed by **estimating semi-variance** over observation points, and then **adjusting a function** over the semi-variance distribution to **interpolate** number of observation/detection over the study area.

### semi-variance

Analysis can be performed at detection scale or at individual scale, *e.g.* in `variogram_intraTransect()`, if `obs = "n_detection"` analysis will be done on the number of detection and group size will not be took into account. However if, `obs = "n_ind"` analysis will be performed on de group size of each detection.

```{r}
sv <- variogram_intraTransect(
  segdata_obs = simul_data,
  obs = "n_detection",
  esw = 0.5,
  # breaks = c(-5,-2,-0.01, 0.001, 0.01, 0.1, 1,seq(5,100,10)),
  intraTransect = TRUE,
  plot = FALSE,
  n_sim = 100
)

# Visualize semi-variance graph
plot(sv$variogram, pch = 19, col = "black")
```

### Adjust function over semi-variance distribution

Different function can be adjusted over semi-variance points, we kept `matern` and `exponential` . It is also possible to estimate nugget by computation or force it to be null (`zero_nugget = TRUE`)

```{r out.width='50%'}

# exponential function
mod_expo_zero_nugg <-fit_variomodel(
  variogram = sv$variogram,
  form = "exponential",
  zero_nugget = TRUE
)
mod_expo_nugg <-fit_variomodel(
  variogram = sv$variogram,
  form = "exponential",
  zero_nugget = FALSE
)

# matern function
mod_mat_zero_nugg <-fit_variomodel(
  variogram = sv$variogram,
  form = "matern",
  zero_nugget = TRUE
)
mod_mat_nugg <-fit_variomodel(
  variogram = sv$variogram,
  form = "matern",
  zero_nugget = FALSE
)
```

Visualization of function adjustment :

```{r out.width="75%", fig.show='hold'}
model_adjust <- semivar_plot(
  sv$variogram,
  distance = 0:100,
  mod_expo_zero_nugg, mod_expo_nugg, mod_mat_zero_nugg, mod_mat_nugg
)

model_adjust$graph_model + theme_bw()

model_adjust$model_synthesis %>% 
  kableExtra::kbl() 
```

### Kriging map prediction

Prediction is estimated with `pois_kriging()`. By default, a grid is created covering the data point distribution range but a custom grid can be used to predict with kriging.

```{r}
pred <- pois_kriging(segdata_obs = simul_data,
                     esw = 0.5,
                     vario_model = mod_mat_zero_nugg,
                     intraTransect = TRUE
                     )
pred %>%
  ggplot(aes(x = X, y = Y, fill = mean_pred)) +
  geom_tile() +
  scale_fill_viridis_c() +
  theme_bw() +
  labs(title = "Detection prediction using kriging")
```
