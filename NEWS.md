<!-- NEWS.md is maintained by https://cynkra.github.io/fledge, do not edit -->

# pelaKrig 0.0.0.9004

* `Kriging_analysis` vignette update
* `semivar_plot()` now handles multiple variomodels with `...`. It have 3 outputs : graph, df of semivariance per distance, df of model parameter estimation.
* `fit_variomodel()` argument `zero_nugget` fixed (was inverted) + doc update


# pelaKrig 0.0.0.9003

* Add `readme`, explain how to install package
- Solve conflict on DESCRIPTION


# pelaKrig 0.0.0.9002

* Initiate a vignette `Kriging_analysis` for the package
* Adapt test of `fit_variomodel()` and `pois_kriging()` with new `simul_data`
* modify `simul_data` to create a more relevant example


# pelaKrig 0.0.0.9001

* `variogram_intraTransect()` have message information from {cli}
* `fit_variomodel()` gains a `zero_nugget` parameter.


# pelaKrig 0.0.0.9000

* Added a `NEWS.md` file to track changes to the package.
