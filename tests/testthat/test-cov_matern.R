test_that("cov_matern works", {
  expect_equal(round(cov_matern(d = 1), 1), 0.5)
  expect_equal(round(cov_matern(d = 2.71), 2), 0.05)
})

test_that("check sill", {
  expect_error(cov_matern(d = 1, sill = c(1, 0)))
  expect_error(cov_matern(d = 1, sill = -1))
})

test_that("check rnage", {
  expect_error(cov_matern(d = 1, range = c(1, 0)))
  expect_error(cov_matern(d = 1, range = -1))
})
